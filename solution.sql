--ACTIVITY

-- a. 

SELECT * FROM artists WHERE name LIKE "%d%";

--b.

SELECT * FROM songs WHERE length < 230;

--c. 

SELECT albums.album_title, song_name, length FROM songs
	JOIN albums ON songs.album_id = albums.id;

--d. 

SELECT * FROM albums 
	JOIN artists ON albums.artist_id = artists.id
	WHERE album_title LIKE "%a%";


--e. 

SELECT * FROM albums ORDER BY album_title DESC
	LIMIT 4;



--f. 

SELECT * FROM songs
	JOIN albums ON songs.album_id = albums.id
	ORDER BY album_title DESC;